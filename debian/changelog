deepin-movie-reborn (5.7.15-2) unstable; urgency=medium

  * debian/patches:
    + Add a patch to define GLAPIENTRY to fix FTBFS on armel/armhf.
  * debian/control:
    + Tighten build dependency versions.
 -- Arun Kumar Pariyar <openarungeek@gmail.com>  Thu, 11 Nov 2021 01:09:53 +0545

deepin-movie-reborn (5.7.15-1) unstable; urgency=medium

  * New upstream release 5.7.15 (Closes: #997202).
  * debian/control
    + Update dependency.
    + Update Standards-Version to 4.6.0.1.
    + Add Clay Stan to Uploaders.
  * debian/patch
    - Deleted the previous patch, because they have
      been incorporated into the source code.
    + Add a patch to remove SetCanShowInUI,
      it has been removed in the new version of mpris player.
    + Add a patch to update manual install path.
    + Add a patch to fix theme does not work.
  * debian/copyright: Update license information.
  * debian/clean: removed,not included in the source code.
  * debian/deepin-movie.install: install manual.
  * debian/libdmr0.1.shlibs: update version.

 -- Clay Stan <claystan97@gmail.com>  Mon, 01 Nov 2021 14:37:13 +0800

deepin-movie-reborn (5.7.6.147-1) unstable; urgency=medium

  [ Tu Qinggang ]
  * New upstream release 5.7.6.147.

  [ Arun Kumar Pariyar ]
  * debian/control:
    + Bump Standards-Version to 4.5.1.
    + Bump debhelper compat to v13.
    + Update build dependency, tighten libdtkwidget-dev to (>=5.2~).
    + Update binary package dependencies.
    + Add Arun Kumar Pariyar to Uploaders.
  * debian/rules:
    + Ensure proper build.
  * debian/patches:
    - Drop use-libdmr.patch and upstream-issue-48.patch.
    + Add fix to build with Qt5.15+ (Closes: #975744).
    + Add patch to use system library instead of src/vendor.
  * debian/upstream: Set upstream metadata fields.
  * debian/clean: Cleanup src/vendor/{dbusextended-qt,mpris-qt}.
  * debian/copyright: Update license information.

 -- Arun Kumar Pariyar <openarungeek@gmail.com>  Sun, 06 Dec 2020 21:03:08 +0545

deepin-movie-reborn (5.0.0-1) unstable; urgency=medium

  * New upstream version 5.0.0.
  * debian/control:
    + Bump Standards-Version to 4.4.1.
  * debian/patches: Add patch to fix FTBFS with new dtkcore.

 -- Boyuan Yang <byang@debian.org>  Tue, 15 Oct 2019 14:57:31 -0400

deepin-movie-reborn (3.2.20-1) unstable; urgency=medium

  * New upstream version 3.2.20

 -- Yanhao Mo <yanhaocs@gmail.com>  Tue, 26 Feb 2019 11:32:06 +0800

deepin-movie-reborn (3.2.18-1) unstable; urgency=medium

  * New upstream release 3.2.18.
  * debian/control:
    + Build-depend on debhelper-compat (= 12) instead of using
      debian/compat.
    + Bump Standards-Version to 4.3.0.

 -- Boyuan Yang <byang@debian.org>  Sun, 06 Jan 2019 10:24:48 -0500

deepin-movie-reborn (3.2.14-1) unstable; urgency=medium

  * New upstream version 3.2.14.
  * debian/control: Drop Build-dependency on libavresample-dev,
    this package is deprecated upstream.
  * debian/patches: Refresh patches.

 -- Boyuan Yang <byang@debian.org>  Tue, 20 Nov 2018 16:30:51 -0500

deepin-movie-reborn (3.2.10-1) unstable; urgency=medium

  * New upstream version 3.2.10.
  * debian/control: Update my uploader email address and use the
    @debian.org one.
  * debian/control: Bump Standards-Version to 4.2.1 (no changes needed).

 -- Boyuan Yang <byang@debian.org>  Sun, 21 Oct 2018 10:13:12 -0400

deepin-movie-reborn (3.2.9-1) unstable; urgency=medium

  * New upstream version 3.2.9.
  * debian/control:
    + Build-depends on libqt5opengl5-desktop-dev to avoid build attempt
      on arm* (Closes: #906691).
    + Bump Standards-Version to 4.2.0 (no changes needed).
    + Use strict version requirement between deepin-movie and libdmr.
  * debian/symbols:
    + Drop libdmr symbols, use shlibs file instead since no other
      application will use libdmr in the foreseeable future.
  * debian/rules: Remove useless dh_install override.

 -- Boyuan Yang <073plan@gmail.com>  Mon, 20 Aug 2018 22:31:56 -0400

deepin-movie-reborn (3.2.8-1) unstable; urgency=medium

  * New upstream version 3.2.8
  * Bump Standards-Version to 4.1.5 (no changes needed).
  * Update symbols file.
  * d/deepin-movie.install: Drop usr/share/dman/deepin-movie/* .

 -- Yanhao Mo <yanhaocs@gmail.com>  Sat, 21 Jul 2018 10:54:14 +0800

deepin-movie-reborn (3.2.7-2) unstable; urgency=medium

  * Reconfirm symbol information using buildd logs.

 -- Yanhao Mo <yanhaocs@gmail.com>  Tue, 12 Jun 2018 10:27:59 +0800

deepin-movie-reborn (3.2.7-1) unstable; urgency=medium

  * New upstream version 3.2.7
    + Run async append job conditionally.
    + libdmr: Support pause on start.

 -- Yanhao Mo <yanhaocs@gmail.com>  Sun, 10 Jun 2018 10:30:00 +0800

deepin-movie-reborn (3.2.5-3) unstable; urgency=medium

  * d/control: Explicitly add Build-Dependency mesa-common-dev
    to deal with FTBFS on armel/armhf platforms.

 -- Boyuan Yang <073plan@gmail.com>  Mon, 21 May 2018 00:19:24 +0800

deepin-movie-reborn (3.2.5-2) unstable; urgency=medium

  * Reconfirm symbol information using buildd logs.

 -- Boyuan Yang <073plan@gmail.com>  Fri, 18 May 2018 09:32:50 +0800

deepin-movie-reborn (3.2.5-1) unstable; urgency=medium

  * New upstream release.
  * Update symbols information of libdmr0.1 using pkg-kde-tools.

 -- Yanhao Mo <yanhaocs@gmail.com>  Sat, 05 May 2018 15:43:01 +0800

deepin-movie-reborn (3.2.4-1) unstable; urgency=medium

  * New upstream release.
  * d/control: add uploader Yanhao Mo <yanhaocs@gmail.com>
  * d/control: change maintainer field back to
    pkg-deepin-devel@lists.alioth.debian.org

 -- Yanhao Mo <yanhaocs@gmail.com>  Fri, 04 May 2018 15:02:05 +0800

deepin-movie-reborn (3.2.3-3) unstable; urgency=medium

  * Bump Standards-Version to 4.1.4 (no changes needed).
  * Use team+pkg-deepin@tracker.debian.org in maintainer field.
  * Mark libdmr0.1 as M-A: same.

 -- Boyuan Yang <073plan@gmail.com>  Fri, 20 Apr 2018 17:25:43 +0800

deepin-movie-reborn (3.2.3-2) unstable; urgency=high

  * Reconfirm symbols for libdmr0.1 using buildd logs.
    Closes: #893622
  * d/control: Apply "wrap-and-sort -abst".
  * d/rules: Use "dh-missing --fail-missing".

 -- Boyuan Yang <073plan@gmail.com>  Fri, 23 Mar 2018 21:44:58 +0800

deepin-movie-reborn (3.2.3-1) unstable; urgency=low

  * Initial release (Closes: #871980)

 -- Yangfl <mmyangfl@gmail.com>  Sat, 17 Mar 2018 10:46:26 +0800
